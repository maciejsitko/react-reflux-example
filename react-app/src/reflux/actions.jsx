var Reflux = require('reflux');

var Actions = Reflux.createActions([
  'getIngredients',
  'postIngedient'
]);

module.exports = Actions;
