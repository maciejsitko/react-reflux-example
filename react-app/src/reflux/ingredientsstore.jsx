var Reflux = require('reflux');
var Actions = require('./actions.jsx');

var HTTP = require('../services/httpservice');

var IngredientsStore = Reflux.createStore({
  listenables: [Actions],
  getIngredients: function() {
    HTTP.get('/ingredients')
    .then(function(json) {
        this.ingredients = json;
        this.fireUpdate();
    }.bind(this));
  },
  postIngedient: function(text) {

    if(!this.ingredients) {
      this.ingredients = [];
    }

    var ingredient = {
      "text" : text,
      "id" : Math.floor(Date.now() / 1000) + text
    };

    this.ingredients.push(ingredient);
    this.fireUpdate();

    HTTP.post('/ingredients',ingredient).then(function(response) {
      return this.getIngredients();
    }.bind(this));

  },
  fireUpdate: function() {
    // Sends to the component -> this.ingredients is the updated data
    // It would be refresh() in non bound way.
    this.trigger('change', this.ingredients);
  }
});

module.exports = IngredientsStore;
