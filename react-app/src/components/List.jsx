var React = require('react');
var ListItem = require('./ListItem.jsx');
var Reflux = require('reflux');
var Actions = require('../reflux/actions.jsx');
var IngredientsStore = require('../reflux/ingredientsstore.jsx');

var List = React.createClass({
    mixins: [Reflux.listenTo(IngredientsStore, 'onChange')],
    // EVENT LISTENER: When you receive new  (confirmed new) data from the Store, fire onChange function.
    getInitialState: function() {
        return {ingredients:[], newText: ""};
    },
    componentWillMount: function() {
      Actions.getIngredients();
    },
    onChange: function(event , ingredients) {
      // EVENT HANDLER: onChange function is fired on IngredientsStore emitting changes.
      this.setState({ingredients: ingredients});
    },
    onClick: function(e) {
      // ACTION: Dispatches new (unconfirmed new) data by a function being mapped by Actions
      // straight to the Store.
      if(this.state.newText) {
        Actions.postIngedient(this.state.newText);
      }
      this.setState({newText: ""});
    },
    onInputChange: function(e) {
      // LOCAL DATA-BINDING
      this.setState({newText: e.target.value});
    },
    render: function() {
        var listItems = this.state.ingredients.map(function(item) {
            return <ListItem key={item.id} ingredient={item.text} />;
        });

        return (
          <div>
            <input placeholder="Add todo" value={this.state.newText} onChange={this.onInputChange}/>
            <button onClick={this.onClick}>Add Item</button>
            <ul>{listItems}</ul>
          </div>
        );
    }
});

module.exports = List;
